const mongoose = require("mongoose");

const userAccountSchema = new mongoose.Schema({
    userAccountFirstName: {
        type: String,
        required: [true, "First name is required"]
    },
    userAccountLastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    userAccountEmail: {
        type: String,
        required: [true, "Email is required"]
    },
    userAccountPassword: {
        type: String,
        required: [true, "Password is required"]
    },
    userAccountMobileNumber: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    userAccountIsAdmin: {
        type: Boolean,
        default: false
    },
    userAccountCreateOn: {
        type: Date,
        default: new Date()
    },
    orders: [
        {
            ordersTotalAmount: {
                type: Number,
                default: 0
            },
            ordersUsersPurchasedOn: {
                type: Date,
                default: new Date()
            },
            ordersProducts: [
                {
                    ordersProductId: {
                        type: String
                    },

                    ordersProductName: {
                        type: String
                    },

                    ordersQuantity: {
                        type: Number,
                        default: 0
                    }

                }
            ]
        }
    ]

})


module.exports = mongoose.model("UserAccount", userAccountSchema);
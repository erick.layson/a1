const express = require("express");
const router = express.Router();
const userAccountControllers = require("../controllers/userAccountControllers");
const auth = require("../auth");



// Route for user registration
router.post("/register", userAccountControllers.registerUserAccount);

// Route for user authentication
 router.post("/login", userAccountControllers.loginUser);

// Route for viewing a specific user detail
router.get("/getprofile", userAccountControllers.getProfile);



/* 
// Route for viewing a specific user detail

*/
router.post("/checkout", userAccountControllers.checkout);
 

/* 
// Route for setting non admin user to admin

*/
router.patch("/setasadmin/:userAccountId", auth.verify, userAccountControllers.setUserToAdmin);











////////////////////////////////////////////////////
module.exports = router;